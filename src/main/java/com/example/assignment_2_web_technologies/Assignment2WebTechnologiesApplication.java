package com.example.assignment_2_web_technologies;

import com.example.assignment_2_web_technologies.data_access.CustomerRepository;
import com.example.assignment_2_web_technologies.models.Customer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment2WebTechnologiesApplication {

    public static void main(String[] args) {

        SpringApplication.run(Assignment2WebTechnologiesApplication.class, args);

        CustomerRepository customerRepository = new CustomerRepository();

        System.out.println(customerRepository.getAllCustomers().size());

        System.out.println(customerRepository.getSpecificCustomer(15).getFirstName());

        System.out.println(customerRepository.getSpecificCustomer("Jennifer").getCustomerId());

        System.out.println(customerRepository.getPageOfCustomers(5, 10).get(4).getFirstName());

        Customer thomas = new Customer(
                531,
                "Thomas",
                "de Jong",
                "the Netherlands",
                "1031AH",
                "0638507270",
                "thomasalexanderdejong@gmail.com"
        );

        customerRepository.addCustomer(thomas);

        System.out.println(customerRepository.getSpecificCustomer("Thomas").getCustomerId());

        System.out.println(customerRepository.getAllCustomers().size());

        Customer hendrik = new Customer(
                531,
                "Hendrik",
                "the First",
                "Germany",
                "1031AH",
                "0633469287",
                "kanyeforpresident@gmail.com"
        );

        System.out.println(customerRepository.getSpecificCustomer(66).getFirstName());


        customerRepository.updateCustomer(66, hendrik);

        System.out.println(customerRepository.getSpecificCustomer(66).getFirstName());

    }

}
