package com.example.assignment_2_web_technologies.data_access;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionHelper {
    public static final String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection connection = null;

    public Connection getConnection() {
        return connection;
    }

    public boolean openConnection() {
        try {
            connection = DriverManager.getConnection(URL);
            return true;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean closeConnection() {
        try {
            connection.close();
            return true;
        } catch (SQLException e) {
            System.out.println("Failed to close SQLite connection.");
            System.out.println(e.getMessage());
            return false;
        }
    }
}
