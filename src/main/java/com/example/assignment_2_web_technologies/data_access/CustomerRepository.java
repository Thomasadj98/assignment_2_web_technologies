package com.example.assignment_2_web_technologies.data_access;

import com.example.assignment_2_web_technologies.models.Customer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CustomerRepository {
    private final ConnectionHelper connectionHelper = new ConnectionHelper();

    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer"
            );

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            connectionHelper.closeConnection();
        }
        return customers;
    }


    public Customer getSpecificCustomer(int customerId) {
        Customer customer = new Customer();
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = ?"
            );

            preparedStatement.setInt(1, customerId);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            connectionHelper.closeConnection();
        }
        return customer;
    }


    public Customer getSpecificCustomer(String name) {
        Customer customer = new Customer();
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE ? OR LastName LIKE ?"
            );

            preparedStatement.setString(1, "%" + name + "%");
            preparedStatement.setString(2, "%" + name + "%");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            connectionHelper.closeConnection();
        }
        return customer;
    }


    // Limit = amount of customers
    // Offset = from which position the x customers picked

    public ArrayList<Customer> getPageOfCustomers(int limit, int offset) {
        ArrayList<Customer> customers = new ArrayList<>();
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer LIMIT ? OFFSET ?"
            );

            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            connectionHelper.closeConnection();
        }
        return customers;
    }


    public void addCustomer(Customer customer) {
        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email)" +
                            "VALUES (?,?,?,?,?,?)"
            );

            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getEmail());

            int ret = preparedStatement.executeUpdate();
            System.out.println("Execute update returns: " + ret);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            connectionHelper.closeConnection();
        }
    }


    public void updateCustomer(int customerId, Customer customer) {
        Customer customerToBeUpdated = getSpecificCustomer(customerId);

        connectionHelper.openConnection();

        try {
            PreparedStatement preparedStatement = connectionHelper.getConnection().prepareStatement(
                    "UPDATE Customer " +
                            "SET FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? " +
                            "WHERE CustomerId = ?"
            );

            if (customer.getFirstName() == null || customer.getFirstName().isEmpty()) {
                preparedStatement.setString(1, customerToBeUpdated.getFirstName());
            } else {
                preparedStatement.setString(1, customer.getFirstName());
            }

            if (customer.getLastName() == null || customer.getLastName().isEmpty()) {
                preparedStatement.setString(2, customerToBeUpdated.getLastName());
            } else {
                preparedStatement.setString(2, customer.getLastName());
            }

            if (customer.getCountry() == null || customer.getCountry().isEmpty()) {
                preparedStatement.setString(3, customerToBeUpdated.getCountry());
            } else {
                preparedStatement.setString(3, customer.getCountry());
            }

            if (customer.getPostalCode() == null || customer.getPostalCode().isEmpty()) {
                preparedStatement.setString(4, customerToBeUpdated.getPostalCode());
            } else {
                preparedStatement.setString(4, customer.getPostalCode());
            }

            if (customer.getPhoneNumber() == null || customer.getPhoneNumber().isEmpty()) {
                preparedStatement.setString(5, customerToBeUpdated.getPhoneNumber());
            } else {
                preparedStatement.setString(5, customer.getPhoneNumber());
            }

            if (customer.getEmail() == null || customer.getEmail().isEmpty()) {
                preparedStatement.setString(6, customerToBeUpdated.getEmail());
            } else {
                preparedStatement.setString(6, customer.getEmail());
            }

            preparedStatement.setInt(7, customerId);

            int ret = preparedStatement.executeUpdate();
            System.out.println("Execute update returns: " + ret);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            connectionHelper.closeConnection();
        }
    }

}
